<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */

$path = base_path() . 'sites/all/themes/training_theme/images/';
?>
<!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
            <![endif]-->
          
            
            <div class="site-main" id="sTop">
              <div class="site-header">
                <div class="container">
                  <div class="row">
                    <div class="col-md-12 text-center">
                      <ul class="social-icons">
                        <li><a href="#" class="fa fa-facebook"></a></li>
                        <li><a href="#" class="fa fa-twitter"></a></li>
                        <li><a href="#" class="fa fa-dribbble"></a></li>
                        <li><a href="#" class="fa fa-linkedin"></a></li>
                      </ul>
                    </div> <!-- /.col-md-12 -->
                  </div> <!-- /.row -->
                </div> <!-- /.container -->
                <div class="main-header">
                  <div class="container">
                      <div id="menu-wrapper">
                        <div class="row">
                          <div class="logo-wrapper col-md-2 col-sm-2">
                            <h1>
                              <a href="#">Flex</a>
                            </h1>
                          </div> <!-- /.logo-wrapper -->
                          <div class="col-md-10 col-sm-10 main-menu text-right">
                            <div class="toggle-menu visible-sm visible-xs"><i class="fa fa-bars"></i></div>
                            <?php if($page['menu']) :?>
                              <ul >
                                <li class="active"><a href="<?php print base_path(); ?>">Home</a></li>
                                <li>
                                  <?php print render($page['menu']); ?>
                                </li>
                              </ul>
                            <?php endif ;?>                                   
                          </div> <!-- /.main-menu -->
                        </div> <!-- /.row -->
                      </div> <!-- /#menu-wrapper -->  
                  </div> <!-- /.container -->
                </div> <!-- /.main-header -->
              </div> <!-- /.site-header -->
              <div class="site-slider">
                <?php if($page['header']) :?>
                  <?php print render($page['header']); ?>
                <?php endif ;?>
              </div> <!-- /.site-slider -->
            </div> <!-- /.site-main -->





<?php 

//$block = module_invoke('service', 'block_view', 'service');
//print render($block['content']);
print render($page['service']); 
?>




<?php print render($page['portfolio']); ?>

<div class="content-section" id="our-team">
  <div class="container">
    <div class="row">
      <div class="heading-section col-md-12 text-center">
        <h2>Our Team</h2>
        <p>Get to know our people and company</p>
      </div> <!-- /.heading-section -->
    </div> <!-- /.row -->
<?php print render($page['team']); ?>        
  </div> <!-- /.container -->
</div> <!-- /#our-team -->












    <div class="content-section" id="contact">
      <div class="container">
        <div class="row">
          <div class="heading-section col-md-12 text-center">
            <h2>Contact</h2>
            <p>Send a message to us</p>
          </div> <!-- /.heading-section -->
        </div> <!-- /.row -->
        <div class="row">
          <div class="col-md-12">
           <div class="googlemap-wrapper">
            <div id="map_canvas" class="map-canvas">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3918.953324496437!2d106.66480431480113!3d10.8148839922954!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9db1d9105526cc66!2sSutrix+Solutions!5e0!3m2!1sen!2s!4v1466142757875" width=100% height=100% frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
          </div> <!-- /.googlemap-wrapper -->
        </div> <!-- /.col-md-12 -->
      </div> <!-- /.row -->
      <div class="row">
        <div class="col-md-7 col-sm-6">
          <p>Flex is a Bootstrap v3.1.1 website design by <span class="blue">template</span><span class="green">mo</span> that can be applied to your website. Slider images and portfolio images are from <a rel="nofollow" href="http://unsplash.com" target="_parent">Unsplash</a> website. Thank you for visiting.<br><br>
            Hic, suscipit, praesentium earum quod ea distinctio impedit ullam deserunt minus dolore quibusdam quis saepe aliquam doloribus voluptatibus eum excepturi. Aenean semper erat neque. Nunc et scelerisque nunc, in adipiscing magna.<br><br>
            Duis ullamcorper tortor tellus. Ut diam libero, ultricies non augue a, mollis congue risus. Fusce a quam eget nisi luctus imperdiet. Consectetur quod at aperiam corporis totam. Nesciunt minima laborum sapiente totam facere unde est cum quia. 
          </p>
          <ul class="contact-info">
            <li>Phone: 020-020-0220</li>
            <li>Email: <a href="mailto:info@company.com">info@company.com</a></li>
            <li>Address: 990 De Best Studio, Fork Street, San Francisco</li>
          </ul>
          <!-- spacing for mobile viewing --><br><br>
        </div> <!-- /.col-md-7 -->
        <div class="col-md-5 col-sm-6">
          <div class="contact-form">
            <form method="post" name="contactform" id="contactform">
              <p>
                <input name="name" type="text" id="name" placeholder="Your Name">
              </p>
              <p>
                <input name="email" type="text" id="email" placeholder="Your Email"> 
              </p>
              <p>
                <input name="subject" type="text" id="subject" placeholder="Subject"> 
              </p>
              <p>
                <textarea name="comments" id="comments" placeholder="Message"></textarea>    
              </p>
              <input type="submit" class="mainBtn" id="submit" value="Send Message">
            </form>
          </div> <!-- /.contact-form -->
        </div> <!-- /.col-md-5 -->
      </div> <!-- /.row -->
    </div> <!-- /.container -->
  </div> <!-- /#contact -->













<br>
CHO NAY LA REGION contact
<br>
<?php print render($page['contact']); ?>
















  <div id="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-xs-12 text-left">
          <span>Copyright &copy; 2014 Company Name</span>
        </div> <!-- /.text-center -->
        <div class="col-md-4 hidden-xs text-right">
          <a href="#top" id="go-top">Back to top</a>
        </div> <!-- /.text-center -->
      </div> <!-- /.row -->
    </div> <!-- /.container -->
  </div> <!-- /#footer -->







<br>
CHO NAY LA REGION footer
<br>
<?php print render($page['footer']); ?>

