
            <div class="content-section" id="services">
              <div class="container">
                <div class="row">
                  <div class="heading-section col-md-12 text-center">
                    <h2>Our Services</h2>
                    <p>We make mobile ready websites for you</p>
                  </div> <!-- /.heading-section -->
                </div> <!-- /.row -->
                <div class="row">

<?php if($nodes) : ?>
<?php 
	foreach($nodes as $node) :
		$description = $node->body['und'][0]['value'];
		$service_backgroundid = field_get_items('node', $node, 'field_service_background')[0]['tid'];
		$service_bg = taxonomy_term_load($service_backgroundid);
		$faid = field_get_items('node', $node, 'field_fa')[0]['tid'];
		$fa = taxonomy_term_load($faid);
?>

                  <div class="col-md-3 col-sm-6">
                    <div class="service-item" id="<?php print $service_bg->name; ?>">
                      <div class="service-icon">
                        <i class=" <?php print $fa->name; ?> "></i>
                      </div> <!-- /.service-icon -->
                      <div class="service-content">
                        <div class="inner-service">
                         <h3><?php print $node->title ; ?></h3>
                         <p><?php print $description; ?></p> 
                       </div>
                     </div> <!-- /.service-content -->
                   </div> <!-- /#service-1 -->
                 </div> <!-- /.col-md-3 -->
<?php endforeach ; ?>
<?php endif ; ?>



         </div> <!-- /.row -->
       </div> <!-- /.container -->
     </div> <!-- /#services -->