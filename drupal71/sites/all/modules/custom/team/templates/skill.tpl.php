<?php if(isset($nodess)) : ?>
<div class="row">
                    <div class="col-md-8 col-sm-6">
                        <p>Aliquam faucibus in dolor sed vestibulum. Sed adipiscing malesuada luctus. Morbi tincidunt, tellus scelerisque scelerisque scelerisque, sapien dui pretium augue, at consectetur sapien tellus vitae nunc. Ut vitae metus quis nulla cursus adipiscing pretium vel dolor. Fusce lacinia accumsan arcu, quis porttitor nisi tincidunt ut. Nunc malesuada nunc eget nunc sollicitudin posuere. Maecenas vitae tortor quis odio hendrerit sagittis.<br><br>
						Etiam tincidunt, magna eu elementum tristique, sapien nisl venenatis lacus, nec sagittis lectus dui eget lorem. Donec in tempus mi. Aenean egestas interdum dolor, et mollis lectus consequat. Mauris ullamcorper, felis sit amet gravida malesuada, nisi sem rhoncus massa, non tempor est erat sit amet diam.<!-- spacing for mobile viewing --><br><br>
						</p>
                    </div> <!-- /.col-md-8 -->
                    <div class="col-md-4 col-sm-6">
                        <ul class="progess-bars">
                        <?php 
    foreach ($nodess as $node) :
        $description = $node->body['und'][0]['value'];
    //echo $description;
    //die();
?>
                            <li>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="<?php print $description; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $description . "%"; ?>;"><?php print $node->title; ?> <?php echo $description . "%"; ?></div>
                                </div>
                            </li>
                            <?php endforeach ; ?>
                        </ul>
                    </div> <!-- /.col-md-4 -->
                </div> <!-- /.row -->
<?php endif; ?>