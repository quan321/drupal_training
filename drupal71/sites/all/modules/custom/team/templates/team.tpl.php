<?php 
	$path = base_path() . 'sites/default/files/';
?>

<?php if(isset($nodes)) : ?>
	<div class="row">
<?php 
	foreach ($nodes as $node) :
		$description = $node->body['und'][0]['value'];
		$field_image_name = $node->field_hinh_anh_team['und'][0]['filename'];
?>

          <div class="team-member col-md-3 col-sm-6">
            <div class="member-thumb">
              <img src="<?php print $path . $field_image_name ; ?>" alt="">
              <div class="team-overlay">
                <h3><?php print $node->title; ?></h3>
                <span><?php print $description; ?></span>
                <ul class="social">
                  <li><a href="<?php print $node->field_link_fb['und'][0]['value']; ?>" class="fa fa-facebook"></a></li>
                  <li><a href="<?php print $node->field_link_tt['und'][0]['value']; ?>" class="fa fa-twitter"></a></li>
                  <li><a href="<?php print $node->field_link_in['und'][0]['value']; ?>" class="fa fa-linkedin"></a></li>
                </ul>
              </div> <!-- /.team-overlay -->
            </div> <!-- /.member-thumb -->
          </div> <!-- /.team-member -->
<?php endforeach ; ?>
<?php endif; ?>
</div> <!-- /.row -->
		
