<?php
	//echo '<pre>';
	//print_r($nodes);
$path = base_path() . 'sites/default/files/';
 ?>




<div class="content-section" id="portfolio">
      <div class="container">
        <div class="row">
          <div class="heading-section col-md-12 text-center">
            <h2>Our Portfolio</h2>
            <p>What we have done so far</p>
          </div> <!-- /.heading-section -->
        </div> <!-- /.row -->
        <div class="row">

<?php if(isset($nodes)) : ?>
<?php 	foreach ($nodes as $node) :
			$description = $node->body['und'][0]['value'];
			//$field_image_id = field_get_items('node', $node, 'field_hinh_anh_portfolio');
			$field_image_name = $node->field_hinh_anh_portfolio['und'][0]['filename'];
			//echo $field_image_name; die();

			
?>

 <div class="portfolio-item col-md-3 col-sm-6">
            <div class="portfolio-thumb">
              <img src="<?php print $path . $field_image_name ; ?>". alt="">
              <div class="portfolio-overlay">
                <h3><?php print $node->title; ?></h3>
                <p><?php print $description; ?></p>
                <a href="<?php print $path . $field_image_name ; ?>" data-rel="lightbox" class="expand">
                  <i class="fa fa-search"></i>
                </a>
              </div> <!-- /.portfolio-overlay -->
            </div> <!-- /.portfolio-thumb -->
          </div> <!-- /.portfolio-item -->

<?php
		endforeach;
	 endif; 
?>






        </div> <!-- /.row -->
      </div> <!-- /.container -->
    </div> <!-- /#portfolio -->